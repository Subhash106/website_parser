<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporations', function (Blueprint $table) {
            $table->id();
            //fields are marked nullable because there possibility a column to be null
            $table->string('name')->nullable();
            $table->string('cin')->nullable();
            $table->string('status')->nullable();
            $table->string('age')->nullable();
            $table->string('registration_number')->nullable();
            $table->string('category')->nullable();
            $table->string('sub_category')->nullable();
            $table->string('coc')->nullable();
            $table->string('roc')->nullable();
            $table->string('number_of_members')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporations');
    }
}
