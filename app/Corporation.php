<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    protected $guarded = [];

    /*
     * corporation and directors one to many relationship
     * */
    public function directors(){
        return $this->hasMany(Directors::class);
    }

    /*
     * corporation and industry one to one relation
     * */

    public function industryClassification(){
        return $this->hasOne(IndustryClassification::class);
    }

    /*
     * corporation and location one to one relation
     * */

    public function locationDetails(){
        return $this->hasOne(LocationDetails::class);
    }

    /*
     * corporation and listing one to one relation
     * */

    public function listingDetails(){
        return $this->hasOne(ListingDetails::class);
    }

    /*
     * corporation and contact one to one relation
     * */

    public function contactDetails(){
        return $this->hasOne(ContactDetails::class);
    }

    /*
    * corporation and FAQs one to one relation
    * */

    public function FAQ(){
        return $this->hasMany(FAQ::class);
    }
}
