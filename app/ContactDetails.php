<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetails extends Model
{
    protected $guarded = [];

    /*
     * corporation and contact one to one relation
     * */

    public function corporation(){
        return $this->belongsTo(ContactDetails::class);
    }
}
