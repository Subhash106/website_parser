<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListingDetails extends Model
{
    protected $guarded = [];

    /*
     * corporation and listing one to one relation
     * */

    public function corporation(){
        return $this->belongsTo(Corporation::class);
    }
}
