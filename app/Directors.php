<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Directors extends Model
{
    protected $guarded = [];

    /*directors and corporation many to one relationship
     * */
    public function corporation(){
        return $this->belongsTo(Corporation::class);
    }
}
