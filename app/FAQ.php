<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model
{
    protected $guarded = [];

    /*
     * FAQs and corporation many to one relation
     * */

    public function corporation(){
        return $this->belongsTo(Corporation::class);
    }
}
