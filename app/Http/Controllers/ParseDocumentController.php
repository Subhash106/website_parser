<?php

namespace App\Http\Controllers;

use App\ContactDetails;
use App\Corporation;
use App\Directors;
use App\FAQ;
use App\IndustryClassification;
use App\ListingDetails;
use App\LocationDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ParseDocumentController extends Controller
{
    public function parse(Request $request)
    {
        $request->validate(
            [
                'url' => 'required|url'
            ]
        );

        $url = $request['url'];
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($handle);
        libxml_use_internal_errors(true); // Prevent HTML errors from displaying
        $doc = new \DOMDocument();
        $doc->loadHTML($html);

        $xpath = new \DOMXPath($doc);

        $corporation = $this->saveCorporationInfo($xpath);
        $this->saveContactInfo($corporation, $xpath);
        $this->saveListingInfo($corporation, $xpath);
        $this->saveLocationInfo($corporation, $xpath);
        $this->saveIndustryInfo($corporation, $xpath);
        $this->saveDirectorsInfo($corporation, $xpath);

        $this->saveFAQs($corporation, $xpath);

        return redirect(url('/'))->with(['success' => 'Website parsed successfully. Please check database.']);
    }

    /*
     * method to store corporation info
     * @param DOMXPath @xpath
     * */
    protected function saveCorporationInfo($xpath)
    {
        $corporation = $xpath->query("//*[@id='companyinformation']")->item(0);
        $rows = $corporation->getElementsByTagName("tr");
        $attrMapping = array(
            'cin' => 'Corporate Identification Number',
            'name' => 'Company Name',
            'status' => 'Company Status',
            'age' => 'Age (Date of Incorporation)',
            'registration_number' => 'Registration Number',
            'category' => 'Company Category',
            'sub_category' => 'Company Subcategory',
            'coc' => 'Class of Company',
            'roc' => 'ROC Code',
            'number_of_members' => 'Number of Members (Applicable only in case of company without Share Capital)'
        );

        $data = array();

        foreach ($rows as $row) {
            if (in_array(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)) {
                $data[array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)] = $row->getElementsByTagName('td')[1]->nodeValue;
            } else {
                $data[array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)] = null;
            }
        }
        //store data to table
        return Corporation::create($data);
    }

    /*
     * method to store contact info
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    protected function saveContactInfo($corporation, $xpath)
    {
        $contactInfo = $xpath->query("//*[@id='contactdetails']")->item(0);
        $rows = $contactInfo->getElementsByTagName("tr");
        $attrMapping = array(
            'email_address' => 'Email Address',
            'office_address' => 'Registered Office'
        );

        $contact = new ContactDetails();

        foreach ($rows as $row) {
            if (in_array(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)) {
                $contact->{array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)} = $row->getElementsByTagName('td')[1]->nodeValue;
            } else {
                $contact->{array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)} = null;
            }
        }

        $corporation->contactDetails()->save($contact);
    }

    /*
     * method to store listing info
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    protected function saveListingInfo($corporation, $xpath)
    {
        $listingInfo = $xpath->query("//*[@id='listingandannualcomplaincedetails']")->item(0);
        $rows = $listingInfo->getElementsByTagName("tr");
        $attrMapping = array(
            'status' => 'Whether listed or not',
            'date_of_last_agm' => 'Date of Last AGM',
            'date_of_balance_sheet' => 'Date of Balance sheet'
        );

        $listing = new ListingDetails();

        foreach ($rows as $row) {
            if (in_array(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)) {
                $listing->{array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)} = $row->getElementsByTagName('td')[1]->nodeValue;
            } else {
                $listing->{array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)} = null;
            }
        }

        $corporation->listingDetails()->save($listing);
    }

    /*
     * method to store contact info
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    protected function saveLocationInfo($corporation, $xpath)
    {
        $locationInfo = $xpath->query("//*[@id='otherinformation']")->item(0);
        $locationTable = $locationInfo->getElementsByTagName("table")[0];
        $rows = $locationTable->getElementsByTagName('tr');

        $attrMapping = array(
            'state' => 'State',
            'district' => 'District',
            'city' => 'City',
            'pin' => 'PIN'
        );

        $location = new LocationDetails();
        foreach ($rows as $row) {
            $attrValue = trim($row->getElementsByTagName('td')[0]->nodeValue);
            if (in_array($attrValue, $attrMapping)) {
                $location->{array_search($attrValue, $attrMapping)} = $row->getElementsByTagName('td')[1]->nodeValue;
            } else {
                $location->{array_search($attrValue, $attrMapping)} = null;
            }
        }

        $corporation->locationDetails()->save($location);
    }

    /*
     * method to store industry info
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    protected function saveIndustryInfo($corporation, $xpath)
    {
        $companyInformation = $xpath->query("//*[@id='industryclassification']")->item(0);
        $rows = $companyInformation->getElementsByTagName("tr");
        $attrMapping = array(
            'section' => 'Section',
            'division' => 'Division',
            'main_group' => 'Main Group',
            'main_class' => 'Main Class'
        );

        foreach ($rows as $row) {
            if (in_array(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)) {
                $data[array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)] = $row->getElementsByTagName('td')[1]->nodeValue;
            } else {
                $data[array_search(trim($row->getElementsByTagName('td')[0]->nodeValue), $attrMapping)] = null;
            }
        }

        $industryClassification = new IndustryClassification();
        foreach ($data as $key=>$datum){
            $industryClassification->{$key} = $datum;
        }
        $corporation->industryClassification()->save($industryClassification);
    }

    /*
     * method to store directors info
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    protected function saveDirectorsInfo($corporation, $xpath)
    {
        $companyInformation = $xpath->query("//*[@id='directors']")->item(0);
        $rows = $companyInformation->getElementsByTagName("tr");
        $attrMapping = array(
            'din',
            'name',
            'designation',
            'date_of_appointment',
            'profile_link'
        );

        foreach ($rows as $key => $row) {
            //skip the table header row
            if ($key === 0)
                continue;

            $tds = $row->getElementsByTagName('td');

            $tdData = array();
            foreach ($tds as $td) {
                $tdData[] = $td->nodeValue;
            }

            Log::info($attrMapping);
            Log::info($tdData);

            $data = array_combine($attrMapping, $tdData);
            $director = new Directors();
            foreach ($data as $keyi => $datum) {
                $director->{$keyi} = $datum;
            }
            $corporation->directors()->save($director);
        }
    }

    /*
     * method to save FAQs
     * @param Corporation $corporation
     * @param DOMXPath $xpath
     * */
    public function saveFAQs($corporation, $xpath){
        $faqInformation = $xpath->query("//*[@id='faq']")->item(0);
        $rows = $faqInformation->getElementsByTagName('div');

        foreach ($rows as $row) {
            //skipping the child divs
            if(isset($row->getElementsByTagName('p')[0]->nodeValue) && isset($row->getElementsByTagName('h3')[0]->nodeValue)){

                $faq = new FAQ();
                 $faq->question = $row->getElementsByTagName('h3')[0]->nodeValue ?? "" ;
                $faq->answer = $row->getElementsByTagName('p')[0]->nodeValue ?? "";

                $corporation->FAQ()->save($faq);
            }
        }
    }
}
