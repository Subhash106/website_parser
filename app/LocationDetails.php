<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationDetails extends Model
{
    protected $guarded = [];

    /*
     * corporation and location one to one relation
     * */

    public function corporation(){
        return $this->belongsTo(Corporation::class);
    }
}
