<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndustryClassification extends Model
{
    protected $guarded = [];

    /*
     * corporation and industry one to one relation
     * */

    public function corporation(){
        return $this->belongsTo(Corporation::class);
    }
}
